import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';

import { connect } from 'react-redux';
import { Container, Content, Button, Item, Input, Toast } from 'native-base';
import { WebBrowser } from 'expo';
import { MonoText } from '../../components/StyledText';
import { goLogin, whoIam, resetAll } from "./actions";
import { createStackNavigator, createAppContainer } from 'react-navigation';
import { AsyncStorage } from 'react-native'

class WelcomeScreen extends React.Component {
  constructor() {
    super()
  }

  static navigationOptions = {
    header: null,
  };

  async _checkLogin() {
    const token = await AsyncStorage.getItem('token')

    whoIam(token)
  }

  componentWillMount() {
    this._checkLogin()
  }

  _validationToken(nextProps, nextState) {
    let validToken = nextProps.loginProps.modLogin.valid_token

    if (validToken !== undefined) {
      if (validToken === 1) {
        nextProps.navigation.navigate('Main')
        return true
      }
    }
    return false
  }

  _validationLogin(nextProps, nextState) {
    let indicate = nextProps.loginProps.modLogin.login

    if (indicate !== undefined) {
      if (indicate.success === 1 && indicate.flag === 0) {
          nextProps.navigation.navigate('Login')
          return true
      }

      if (nextProps.loginProps.modLogin.login.msg !== undefined) {
        alert(nextProps.loginProps.modLogin.login.msg)
      }
    }

    return false
  }

  _validationError(nextProps, nextState) {
    let errMsg = nextProps.loginProps.modLogin.errMsg

    if (errMsg !== undefined) {
      Toast.show({
        text: errMsg,
        buttonText: "OK",
        duration: 3000
      })
      
      resetAll()
    }

    return false
  }

  shouldComponentUpdate(nextProps, nextState) {
    this._validationLogin(nextProps, nextState)
    this._validationToken(nextProps, nextState)
    this._validationError(nextProps, nextState)

    return true
  }

  render() {
    // console.log(this.props)
    return (
      <View style={styles.container}>

        <ScrollView style={styles.container} contentContainerStyle={styles.contentContainer}>

          <View style={styles.welcomeContainer}>

            <Image
              source={
                __DEV__
                  ? require('../../assets/images/robot-dev.png')
                  : require('../../assets/images/robot-prod.png')
              }
              style={styles.welcomeImage}
            />
          </View>

          <View style={styles.getStartedContainer}>

            <Text style={styles.getStartedText}>Welcome</Text>

            <View style={[styles.codeHighlightContainer, styles.homeScreenFilename]}>
              <MonoText style={styles.codeHighlightText}>People who often forget </MonoText>
            </View>

            <ComponentInput/>
            
          </View>
          
        </ScrollView>

        <View style={styles.tabBarInfoContainer}>
          <Text style={styles.tabBarInfoText}>Key Pass Application</Text>

          <View style={[styles.codeHighlightContainer, styles.navigationFilename]}>
            <MonoText style={styles.codeHighlightText}>Myles Waspodov</MonoText>
          </View>
        </View>
      </View>
    );
  }
}

class ComponentInput extends React.Component {
  constructor() {
    super()
    this.state = {
      email: '',
    }
  }

  _changeEmail(value) {
    this.setState({
      email: value
    });
  }

  _submitEmail() {
    goLogin({
      email: this.state.email
    })
  }

  render() {
    return(
        <View style={styles.getStartedContainer}>
          <Item regular style={{ marginTop: 20 }}>
            <Input placeholder="Input your email" value={ this.state.email } onChangeText={ this._changeEmail.bind(this) } />
          </Item>

          <Button block info style={{ marginTop: 10 }} onPress={ this._submitEmail.bind(this) }>
            <Text style={{ color: "white" }}> Send </Text>
          </Button>

        </View>
    );
  }
}

const mapStateToProps = (state) => {
    // console.log(state);
    return { loginProps: state }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  developmentModeText: {
    marginBottom: 20,
    color: 'rgba(0,0,0,0.4)',
    fontSize: 14,
    lineHeight: 19,
    textAlign: 'center',
  },
  contentContainer: {
    paddingTop: 30,
  },
  welcomeContainer: {
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 20,
  },
  welcomeImage: {
    width: 100,
    height: 80,
    resizeMode: 'contain',
    marginTop: 3,
    marginLeft: -10,
  },
  getStartedContainer: {
    alignItems: 'center',
    marginHorizontal: 20,
  },
  homeScreenFilename: {
    marginVertical: 7,
  },
  codeHighlightText: {
    color: 'rgba(96,100,109, 0.8)',
  },
  codeHighlightContainer: {
    backgroundColor: 'rgba(0,0,0,0.05)',
    borderRadius: 3,
    paddingHorizontal: 4,
  },
  getStartedText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    lineHeight: 24,
    textAlign: 'center',
  },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
  tabBarInfoText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    textAlign: 'center',
  },
  navigationFilename: {
    marginTop: 5,
  },
  helpContainer: {
    marginTop: 15,
    alignItems: 'center',
  },
  helpLink: {
    paddingVertical: 15,
  },
  helpLinkText: {
    fontSize: 14,
    color: '#2e78b7',
  },
})

export default connect(mapStateToProps)(WelcomeScreen);